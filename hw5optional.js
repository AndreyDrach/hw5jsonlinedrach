// "use strict";

function createNewUser() {
  const newUser = {
    firstName: prompt("Enter your name"),
    lastName: prompt("Enter your last name"),
    setNewFirstName(newFName) {
      Object.defineProperty(newUser, "firstName", { writable: true, configurable: false });
      this.firstName = newFName;
        },
    setNewLastName(newLName) {
      Object.defineProperty(newUser, "lastName", {writable: true, configurable: false});
      this.lastName = newLName;
         },
  };
  Object.defineProperty(newUser, "firstName", {
    writable: false,
    configurable: true,
  });
  Object.defineProperty(newUser, "lastName", {
    writable: false,
    configurable: true,
  });

  return newUser;
}
console.log(createNewUser());



// // Тестирование
let myUser = createNewUser(); // Создали объект
myUser.firstName = "TEST1"; // Попытались напрямую перезаписать имя на другое. 
myUser.lastName = "TEST2"; // Попытались напрямую перезаписать фамилию на другую. 
console.log(myUser); // Не смотря на попытки перезаписать имя / фамилию - они остались старыми, так как writable: false

let myUserFunction = createNewUser();
myUserFunction.setNewFirstName("TEST3"); // Пытаемся перезаписать имя через метод
myUserFunction.setNewLastName("TEST4"); // Пытаемся перезаписать фамилию через метод
console.log(myUserFunction); // Имя и фамилия теперь новые, потому что configurable: true и через повторные вызовы Object.defineProperty они перезаписываются, а напрямую - нет.
